/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package edu.duke.runya;

import java.io.IOException;
import java.net.URL;

import edu.duke.runya.controller.NumButtonController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/*

public class App {
    public String getGreeting() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        System.out.println(new App().getGreeting());
   
}
*/

//public class App extends Application {

public class App extends Application {

  @Override
   public void start(Stage stage) throws IOException{


    
    URL xmlResource = getClass().getResource("/ui/calc-monolithic.xml");
    FXMLLoader loader = new FXMLLoader(xmlResource);
    //loader.setController(new NumButtonController());
    
    GridPane gp = loader.load();
  
    Scene scene = new Scene(gp, 640, 480);

    URL cssResource = getClass().getResource("/ui/calcbuttons.css");

    scene.getStylesheets().add(cssResource.toString());

    stage.setScene(scene);
    stage.show();
  }

  
}

/*
 * 
 * String javaVersion = System.getProperty("java.version");
 * String javafxVersion = System.getProperty("javafx.version");
 * Label l = new Label("Hello, JavaFX " + javafxVersion +
 * ", running on Java " +
 * javaVersion + ".");
 * Scene scene = new Scene(new StackPane(l), 640, 480);
 * stage.setScene(scene);
 * stage.show();
 */

/*
 * 
 * GridPane gp = new GridPane();
 * String[] labels = new String[] {
 * "+", "-", "*", "/",
 * "7", "8", "9",
 * "4", "5", "6",
 * "1", "2", "3",
 * "." };
 * int rows[] = new int[] {
 * 0, 0, 0, 0,
 * 1, 1, 1,
 * 2, 2, 2,
 * 3, 3, 3,
 * 4 };
 * int cols[] = new int[] {
 * 0, 1, 2, 3,
 * 0, 1, 2,
 * 0, 1, 2,
 * 0, 1, 2,
 * 2 };
 * /*
 * 
 * for (int i = 0; i < labels.length; i++) {
 * Button b = new Button(labels[i]);
 * gp.add(b, cols[i], rows[i]);
 * }
 * 
 * gp.add(new Button("0"), 0, 4, 2, 1);
 * gp.add(new Button("E\nn\nt\ne\nr"), 3, 1, 1, 3);
 */

/*
 * for (int i = 0; i < labels.length; i++) {
 * Button b = new Button(labels[i]);
 * gp.add(b, cols[i], rows[i]);
 * b.setMaxWidth(Double.MAX_VALUE);
 * b.setMaxHeight(Double.MAX_VALUE);
 * }
 * 
 * for (int i =0; i < 4; i++) {
 * ColumnConstraints cc = new ColumnConstraints();
 * cc.setPercentWidth(25);
 * gp.getColumnConstraints().add(cc);
 * }
 * for(int i =0; i < 5; i++) {
 * RowConstraints rc = new RowConstraints();
 * rc.setPercentHeight(20);
 * gp.getRowConstraints().add(rc);
 * }
 * 
 * Button b = new Button("0");
 * b.setMaxWidth(Double.MAX_VALUE);
 * b.setMaxHeight(Double.MAX_VALUE);
 * gp.add(b, 0, 4, 2, 1);
 * b = new Button("E\nn\nt\ne\nr");
 * b.setMaxWidth(Double.MAX_VALUE);
 * b.setMaxHeight(Double.MAX_VALUE);
 * gp.add(b, 3, 1, 1, 3);
 * 
 * Scene scene = new Scene(gp, 640, 480);
 * 
 * stage.setScene(scene);
 * stage.show();
 * 
 */
